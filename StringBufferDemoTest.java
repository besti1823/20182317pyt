import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class StringBufferDemoTest {
    StringBuffer str12 = new StringBuffer("StringBuffer");
    StringBuffer str24 = new StringBuffer("StringBufferStringBuffer");
    StringBuffer str36 = new StringBuffer("StringBufferStringBufferStringBuffer");

    @Test
    public void testcharAt(){
        Assert.assertEquals('S', str12.charAt(0));
        Assert.assertEquals('g', str24.charAt(5));
    }

    @Rule
    public ExpectedException thrown= ExpectedException.none();
    @Test
    public void testcharAtException(){
        thrown.expect(StringIndexOutOfBoundsException.class);
        str36.charAt(40);
    }

    @Test
    public void testlength(){
        Assert.assertEquals(12, str12.length());
        Assert.assertEquals(24, str24.length());
        Assert.assertEquals(36, str36.length());
    }

    @Test
    public void testcapacity(){
        Assert.assertEquals(28, str12.capacity());
        Assert.assertEquals(40, str24.capacity());
        Assert.assertEquals(52, str36.capacity());
        Assert.assertEquals(82,str24.append("StringBufferStringBuffer").capacity());
    }

    @Test
    public void testindex(){
        Assert.assertEquals(1, str12.indexOf("t"));
        Assert.assertEquals(8, str24.indexOf("f"));
        Assert.assertEquals(2, str36.indexOf("r"));
    }
}
