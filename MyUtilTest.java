public class MyUtilTest {
    public static void main(String[] args) {
        //测试正常情况
        if(MyUtil.percentage2fivegrade(55) != "不及格")
            System.out.println("test failed!");
        else if(MyUtil.percentage2fivegrade(65) != "及格")
            System.out.println("test failed!");
        else if(MyUtil.percentage2fivegrade(75) != "中等")
            System.out.println("test failed!");
        else if(MyUtil.percentage2fivegrade(85) != "良好")
            System.out.println("test failed!");
        else if(MyUtil.percentage2fivegrade(95) != "优秀")
            System.out.println("test failed!");
        else
            System.out.println("test passed!");
    }
}
