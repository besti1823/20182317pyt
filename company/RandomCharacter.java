public class RandomCharacter {
    public static char gerRandomCharacter(char ch1,char ch2){
        return(char)(ch1+Math.random()*(ch2-ch1+1));
    }
    public static char getRandomLowerCaseLetter(){
        return gerRandomCharacter('a','z');
    }
    public static char getRandomUpperCaseLetter(){
        return gerRandomCharacter('A','Z');

}}
