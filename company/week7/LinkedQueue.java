package week7;
public class LinkedQueue<T> implements Queue<T>{

    private LinearNode<T> front;
    private LinearNode<T> rear;
    private int count=0;

    public LinkedQueue() {
        LinearNode<T> New=new LinearNode<>();
        front=New;
        rear=New;
    }

    public LinkedQueue(T initialElement) {
        LinearNode<T> New=new LinearNode<>(initialElement);
        front=New;
        rear=New;
        count++;
    }

    @Override
    public void enqueue(T element) {
        if (isEmpty()){
            rear.setElement(element);
        }
        else {
            LinearNode<T> New=new LinearNode<>(element);
            rear.setNext(New);
            rear=rear.getNext();
        }
        count++;
    }

    @Override
    public T dequeue() {
        count--;
        if (isEmpty()){

            return  null;
        }
        else {
            LinearNode<T> result=front;
            front=front.getNext();
            return result.getElement();
        }
    }

    @Override
    public T first() {
        if (isEmpty()){
            return  null;
        }
        else {
            return front.getElement();
        }
    }

    @Override
    public boolean isEmpty() {
        if (front==rear&&rear.getElement()==null){
            return true;
        }
        else {
            return false;
        }
    }
    //#########################################
    @Override
    public int size() {
        return count;
    }
    //#########################################
    @Override
    public String toString() {
        LinearNode<T> temp=front;
        String result="";
        if (isEmpty()){
            return null;
        }
        else {
            while (temp!=null){
                result=result+temp.getElement()+" ";
                temp=temp.getNext();//#########################################
            }//#########################################
        }//#########################################
        return result;
    }
}