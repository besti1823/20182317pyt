package week7;
public class ArrayStackTest {
    public static void main(String[] args)  {
        ArrayStack stack=new ArrayStack();
        stack.push("Peng");
        stack.push("Yan");
        stack.push("Tai");//#########################################
        System.out.println("输出stack："+stack);//#########################################
        System.out.println("输出栈元素的个数："+stack.size());//#########################################
        System.out.println("输出栈顶："+stack.peek());//#########################################
        System.out.println("输出栈顶并删除："+stack.pop()+"\n");//#########################################
        System.out.println("输出stack："+stack);
        System.out.println("是否为空:"+stack.isEmpty());
        System.out.println("输出栈元素的个数："+stack.size());
        System.out.println("输出栈顶："+stack.peek());
    }
}