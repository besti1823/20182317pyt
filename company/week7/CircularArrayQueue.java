package week7;
public class CircularArrayQueue<T> implements Queue<T>{
    private final int DEFAULT_CAPACITY = 10;
    private int front, rear, count,c;
    private T[] queue;

    public CircularArrayQueue()
    {
        front = rear = count = c = 0;
        queue = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    public void enqueue (T element)
    {
        if (size() == queue.length)
            expandCapacity();

        queue[rear] = element;
        rear = (rear+1) % queue.length;

        count++;
        c++;
    }
    public void expandCapacity()
    {
        T[] larger = (T[])(new Object[queue.length *2]);

        for(int scan=0; scan < count; scan++)
        {
            larger[scan] = queue[front];
            front=(front+1) % queue.length;
        }

        front = 0;
        rear = count;
        queue = larger;
    }
    public T dequeue()
    {
        if (isEmpty())
            System.out.println("空队列");

        T result = queue[front];
        queue[front] = null;
        front = (front+1) % queue.length;

        c--;
        return result;
    }


    //#########################################
    public T first() {
        if (isEmpty())
            System.out.println("空队列");

        return queue[front];
    }
    //#########################################
    public boolean isEmpty()
    {
        return (count == 0);
//#########################################
    }
    public int size()
    {
        if(count == 10)
            System.out.println("队列为满队列");
        return count;
    }
    //#########################################
    public int size2()
    {
        if(c == 0)
            System.out.println("队列为空队列");
        return c;
    }
    //#########################################
    public String toString()
    {
        String result = "";
        int scan = 0;
//#########################################
        while(scan < count)
        {
            if(queue[scan]!=null)
            {
                result += queue[scan].toString()+" ";
            }
            scan++;
        }
//#########################################
        return result;
    }
//#########################################
//#########################################
}