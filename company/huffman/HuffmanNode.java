package week10;
public class HuffmanNode<T> implements Comparable<HuffmanNode<T>>{
    private double weight; //权值
    private T word; // 字母
    private HuffmanNode left;
    private HuffmanNode right;
    private HuffmanNode parent;
    String code; // 存储最后的编码
    /*************/
    public HuffmanNode(double weight,T word){
        this.weight = weight;
        this.word = word;
        this.code = "";
    }
    /*************/
    public String getCode() {
        return code;
    }
    /****************/
    public void setCode(String str) {
        code = str;
    }
    /*****************/
    public double getWeight() {
        return weight;
    }
    /***************/
    public void setWeight(double weight) {
        this.weight = weight;
    }
    /***************/
    public T getWord() {
        return word;
    }
    /*****************/
    public void setWord(T word) {
        this.word = word;
    }
    /*************/
    public HuffmanNode getLeft() {
        return left;
    }

    public void setLeft(HuffmanNode left) {
        this.left = left;
    }

    public HuffmanNode getRight() {
        return right;
    }

    public void setRight(HuffmanNode right) {
        this.right = right;
    }

    public HuffmanNode getParent() {
        return parent;
    }

    public void setParent(HuffmanNode parent) {
        this.parent = parent;
    }

    @Override
    public String toString() {
        return word + " 权值： " + weight + " 左孩子： " + left + " 右孩子： " + right;
    }

    @Override
    public int compareTo(HuffmanNode<T> o) {
        if (this.getWeight() > o.getWeight()){
            return -1;
        }
        else if (this.getWeight() < o.getWeight()){
            return 1;
        }
        return 0;
    }
}/*
      List<HuffmanNode> list1;
        list1 = HuffmanTree.BFS(root);
        List<String> list2 = new ArrayList<>(); // 用于存储每个结点的字母
        List<String> list3 = new ArrayList<>(); // 用于存储每个结点的编码
        for (int i = 0;i < list1.size();i++){
            if (list1.get(i).getWord() != null){
                list2.add(String.valueOf(list1.get(i).getWord()));
                list3.add(list1.get(i).getCode());
            }
        }
/*************************/
/*
        Collections.sort(list2);
                for (int i = 0;i < list3.size();i++){
        System.out.println(list2.get(i) + "的编码为： " + list3.get(i));
        }
        String result = "";
        for (int i = 0;i < a.length();i++){
        for (int j = 0; j < list2.size();j++){
        if (a.charAt(i) == list2.get(j).charAt(0)){
        result += list3.get(j);
        }
        }
        }
        System.out.println("文件编码结果为： " + result);
        System.out.println("解码结果");

        // 进行解码
        List<String> list4 = new ArrayList<>();
        for (int i = 0;i < result.length();i++){
        list4.add(result.charAt(i) + "");
        }
        String temp = "";
        String temp1 = "";
        while (list4.size() > 0){
        temp += "" + list4.get(0);
        list4.remove(0);
        for (int i = 0;i < list3.size();i++){
        if (temp.equals(list3.get(i))){
        temp1 += "" + list2.get(i);
        temp = "";
        }
        }
        }
 */