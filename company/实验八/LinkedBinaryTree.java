package 实验八;
import week7.LinkedQueue;
import 实验七.EmptyCollectionException;

import java.util.ArrayList;
import java.util.Iterator;

public class LinkedBinaryTree<T> implements BinaryTree<T>
{
    protected BTNode<T> root;
    public LinkedBinaryTree()
    {
        root=null;
    }
    public LinkedBinaryTree(T element)
    {
        root = new BTNode<T>(element);
    }
    public LinkedBinaryTree(T element,LinkedBinaryTree<T> left,LinkedBinaryTree<T> right)
    {
        root=new BTNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }
    public T getRootElement()
    {
        if (root==null)
            throw new EmptyCollectionException("Get root operation"+"failed.The tree is empty.");
        return root.getElement();
    }
    public LinkedBinaryTree<T> getLeft()
    {
        if (root==null)
            throw new EmptyCollectionException("Get root operation"+"failed.The tree is empty.");
        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root=root.getLeft();
        return result;
    }
    public T find(T target)
    {
        BTNode<T> node = null;
        if (root!=null)
            node=root.find(target);
        if(node==null)
            throw new EmptyCollectionException("Find operation failed."+"No such element in tree.");

        return node.getElement();
    }
    public int size()
    {
        int result=0;
        if (root!=null)
            result=root.count();
        return result;
    }
    public ArrayList<T> inOrder()
    {
        ArrayList<T> iter = new ArrayList<T>();
        if (root != null)
            root.inorder(iter);
        return iter;
    }

    public ArrayList<T> levelorder()
    {
        LinkedQueue<BTNode<T>> queue = new LinkedQueue<BTNode<T>>();
        ArrayList<T> iter = new ArrayList<T>();

        if (root != null)
        {
            queue.enqueue(root);
            while (!queue.isEmpty())
            {
                BTNode<T> current = queue.dequeue();
                iter.add(current.getElement());
                if (current.getLeft()!=null)
                    queue.enqueue((BTNode<T>) current.getLeft());
                if (current.getRight()!=null)
                    queue.enqueue((BTNode<T>) current.getRight());
            }
        }
        return iter;
    }
    public ArrayList<T> iterator()
    {
        return inOrder();
    }
    public LinkedBinaryTree<T> getRight()
    {
        if (root==null)
            throw new EmptyCollectionException("Get root operation"+"failed.The tree is empty.");
        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root=root.getRight();
        return result;
    }
    public boolean contains(T target)
    {
        BTNode a=root.find(target);
        if(a!=null){
            return true;
        }
        return false;
    }
    public boolean isEmpty()
    {
        if(root.count()==0){
            return true;
        }
        return false;
    }
    public String toString() {
        return super.toString();
    }
    @Override
    public Iterator<T> inorder() {
        ArrayList<T> iter=new ArrayList<>();
        if(root!=null){
            root.inorder(iter);
        }
        return iter;
    }
    public ArrayList<T> preorder() {
        ArrayList<T> iter=new ArrayList<>();
        if(root!=null){
            root.preorder(iter);
        }
        return iter;
    }
    public ArrayList<T> postorder() {
        ArrayList<T> iter=new ArrayList<>();
        if(root!=null){
            root.postorder(iter);
        }
        return iter;
    }
}
/*

    public void testInorder() {
        assertEquals("[1]",a.inOrder().toString());
        assertEquals("[2]",b.inOrder().toString());
        assertEquals("[1, 3, 2]",c.inOrder().toString());
        assertEquals("[4]",d.inOrder().toString());
        assertEquals("[1, 3, 2, 5, 4]",e.inOrder().toString());
    }

    public void testIsEmpty() {
        assertEquals(false,a.isEmpty());
        assertEquals(false,b.isEmpty());
        assertEquals(false,c.isEmpty());
        assertEquals(false,d.isEmpty());
        assertEquals(false,e.isEmpty());
    }


    public void testPreorder() {
        assertEquals("[1]",a.preorder().toString());
        assertEquals("[2]",b.preorder().toString());
        assertEquals("[3, 1, 2]",c.preorder().toString());
        assertEquals("[4]",d.preorder().toString());
        assertEquals("[5, 1, 3, 2, 4]",e.preorder().toString());

    }
   }
    public ArrayList<T> iterator()
    {
        return inOrder();
    }
    public LinkedBinaryTree<T> getRight()
    {
        if (root==null)
            throw new EmptyCollectionException("Get root operation"+"failed.The tree is empty.");
        LinkedBinaryTree<T> result = new LinkedBinaryTree<T>();
        result.root=root.getRight();
        return result;
    }
    public boolean contains(T target)
    {
        BTNode a=root.find(target);
        if(a!=null){
            return true;
        }
        return false;
    public void testPostorder() {
        assertEquals("[1]",a.postorder().toString());
        assertEquals("[2]",b.postorder().toString());
        assertEquals("[1, 2, 3]",c.postorder().toString());
        assertEquals("[4]",d.postorder().toString());
        assertEquals("[1, 3, 2, 4, 5]",e.postorder().toString());
    }
}
 */