package 实验八;
import java.util.Scanner;

public class DecisionTree {
    public static void main(String[] args) throws EmptyCollectionException {
        LinkedBinaryTree<String> ques12 = new LinkedBinaryTree<String>("别出去了啦!");
        LinkedBinaryTree<String> ques13 = new LinkedBinaryTree<String>("睡觉吧!");

        LinkedBinaryTree<String> ques9 = new LinkedBinaryTree<String>("打游戏吧!");
        LinkedBinaryTree<String> ques8 = new LinkedBinaryTree<String>("出去见面吧");
        LinkedBinaryTree<String> ques10 = new LinkedBinaryTree<String>("算了吧?", ques12, ques13);
        LinkedBinaryTree<String> ques11 = new LinkedBinaryTree<String>("给爷爬!!!");

        LinkedBinaryTree<String> ques1 = new LinkedBinaryTree<String>("今天适宜出行");
        LinkedBinaryTree<String> ques2 = new LinkedBinaryTree<String>("在家玩吧!");
        LinkedBinaryTree<String> ques6 = new LinkedBinaryTree<String>("出去可有要事?", ques10, ques11);
        LinkedBinaryTree<String> ques7 = new LinkedBinaryTree<String>("有没有要去的地方?", ques8, ques9);

        LinkedBinaryTree<String> ques3 = new LinkedBinaryTree<String>("温度是否适宜?", ques1, ques2);
        LinkedBinaryTree<String> ques5 = new LinkedBinaryTree<String>("温度是否适宜?", ques7, ques6);

        LinkedBinaryTree<String> ques4 = new LinkedBinaryTree<String>("天气是否晴朗?", ques3, ques5);

        LinkedBinaryTree<String> quesPrint = ques4;
        Scanner scan = new Scanner(System.in);
        String rpy;
//        boolean brk = false;

//        String able1 = "Y";
//        String able2 = "y";
//        String able3 = "N";
//        String able4 = "n";

        for(;;){
            System.out.println(quesPrint.getRootElement());
            if(quesPrint.root.left == null && quesPrint.root.right == null)
                break;

            rpy = scan.nextLine();
            for (;;){
                if(rpy.compareTo("y") == 0 || rpy.compareTo("Y") == 0 || rpy.compareTo("n") == 0 || rpy .compareTo("N") == 0)
                    break;
                else
                    System.out.println("Entered Error! Please enter again");
            }
            if(rpy.compareTo("y") == 0 || rpy.compareTo("Y") == 0 )
                quesPrint = quesPrint.getLeft();
            if(rpy.compareTo("n") == 0 || rpy .compareTo("N") == 0)
                quesPrint = quesPrint.getRight();
        }
        System.out.println("玩得开心");
    }
}