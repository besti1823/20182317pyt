package 实验八;
import org.hamcrest.internal.ArrayIterator;
import java.util.ArrayList;
public class BTNode<T> {
    protected T element;
    protected BTNode<T> left;
    protected BTNode<T> right;
    public BTNode (T element)
    {
        this.element = element;
        left = right = null;
    }
    public T getElement()
    {
        return element;
    }
    public void setElement (T element)
    {
        this.element = element;
    }
    public BTNode<T> getLeft()
    {
        return left;
    }
    public void setLeft (BTNode<T> left)
    {
        this.left = left;
    }
    public BTNode<T> getRight()
    {
        return right;
    }
    public void setRight (BTNode<T> right)
    {
        this.right = right;
    }
    public BTNode<T> find (T target)
    {
        BTNode<T> result = null;
        if (element.equals(target))
            result = this;
        else
        {
            if (left != null)
                result = left.find(target);
            if (result == null && right != null)
                result = right.find(target);
        }
        return result;
    }
    public int count()
    {
        int result = 1;

        if (left != null)
            result += left.count();

        if (right != null)
            result += right.count();
        return result;
    }
    public void inorder ( ArrayIterator iter)
    {
        if (left != null)
            left.inorder (iter);

        if (right != null)
            right.inorder (iter);
    }
    public void preorder ( ArrayIterator iter) {

        if(left!=null)
            left.preorder(iter);
        if (right != null)
            right.preorder(iter);
    }
    public void postorder ( ArrayIterator iter) {
        if(left != null)
            left.postorder(iter);
        if(right != null)
            right.postorder(iter);

    }
}/*

    public void testInorder() {
        assertEquals("[1]",a.inOrder().toString());
        assertEquals("[2]",b.inOrder().toString());
        assertEquals("[1, 3, 2]",c.inOrder().toString());
        assertEquals("[4]",d.inOrder().toString());
        assertEquals("[1, 3, 2, 5, 4]",e.inOrder().toString());
    }

    public void testIsEmpty() {
        assertEquals(false,a.isEmpty());
        assertEquals(false,b.isEmpty());
        assertEquals(false,c.isEmpty());
        assertEquals(false,d.isEmpty());
        assertEquals(false,e.isEmpty());
    }


    public void testPreorder() {
        assertEquals("[1]",a.preorder().toString());
        assertEquals("[2]",b.preorder().toString());
        assertEquals("[3, 1, 2]",c.preorder().toString());
        assertEquals("[4]",d.preorder().toString());
        assertEquals("[5, 1, 3, 2, 4]",e.preorder().toString());

    }

    public void testPostorder() {
        assertEquals("[1]",a.postorder().toString());
        assertEquals("[2]",b.postorder().toString());
        assertEquals("[1, 2, 3]",c.postorder().toString());
        assertEquals("[4]",d.postorder().toString());
        assertEquals("[1, 3, 2, 4, 5]",e.postorder().toString());
    }
}
 */