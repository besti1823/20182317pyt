package 实验八;
import java.util.Iterator;
public interface BinaryTree<T> extends Iterable<T> {
    public T getRootElement() throws Exception;
    public BinaryTree<T> getLeft() throws Exception;
    public BinaryTree<T> getRight() throws Exception;
    public boolean contains(T target) throws Exception;
    public T find(T target) throws Exception;
    public boolean isEmpty();
    public int size();
    public String toString();
    public Iterator<T> preorder();
    public Iterator<T> inorder();
    public Iterator<T> postorder();
    public Iterator<T> levelorder() throws Exception;
}/*

    public void testInorder() {
        assertEquals("[1]",a.inOrder().toString());
        assertEquals("[2]",b.inOrder().toString());
        assertEquals("[1, 3, 2]",c.inOrder().toString());
        assertEquals("[4]",d.inOrder().toString());
        assertEquals("[1, 3, 2, 5, 4]",e.inOrder().toString());
    }

    public void testIsEmpty() {
        assertEquals(false,a.isEmpty());
        assertEquals(false,b.isEmpty());
        assertEquals(false,c.isEmpty());
        assertEquals(false,d.isEmpty());
        assertEquals(false,e.isEmpty());
    }


    public void testPreorder() {
        assertEquals("[1]",a.preorder().toString());
        assertEquals("[2]",b.preorder().toString());
        assertEquals("[3, 1, 2]",c.preorder().toString());
        assertEquals("[4]",d.preorder().toString());
        assertEquals("[5, 1, 3, 2, 4]",e.preorder().toString());

    }

    public void testPostorder() {
        assertEquals("[1]",a.postorder().toString());
        assertEquals("[2]",b.postorder().toString());
        assertEquals("[1, 2, 3]",c.postorder().toString());
        assertEquals("[4]",d.postorder().toString());
        assertEquals("[1, 3, 2, 4, 5]",e.postorder().toString());
    }
}
 */