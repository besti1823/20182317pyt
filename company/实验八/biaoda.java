package 实验八;
import java.util.Scanner;
import java.util.EmptyStackException;
import java.util.Stack;
public class biaoda {
    public static String infixToPostfix(String infix) {
        Stack<Character> op = new Stack<Character>();
        StringBuilder postfixStr = new StringBuilder("");
        char[] prefixs = infix.trim().toCharArray();
        Character ch;
        for (int i = 0; i < prefixs.length; i++) {
            ch = prefixs[i];
            if (ch >= '0' && ch <= '9') {
                postfixStr.append(ch);
                continue;
            }
            if ('(' == ch) {
                op.push(ch);
                continue;
            }
            if ('+' == ch || '-' == ch) {
                while (!op.empty() && (op.peek() != '(')) {
                    postfixStr.append(op.pop());
                }
                op.push(ch);
                continue;
            }
            if ('*' == ch || '/' == ch) {
                while (!op.empty() && (op.peek() == '*' || op.peek() == '/')) {
                    postfixStr.append(op.pop());
                }
                op.push(ch);
                continue;
            }
            if (')' == ch) {
                while (!op.empty() && op.peek() != '(') {
                    postfixStr.append(op.pop());
                }
                op.pop();
                continue;
            }
        }
        while (!op.empty())
            postfixStr.append(op.pop());
        return postfixStr.toString();
    }

    public static int sumPostfix(String postfix){
        Stack<Integer> values;
        int result=0;
        try {
            values = new Stack<Integer>();
            char [] postfixs =postfix.trim().toCharArray();
            Character ch;
            for(int i=0;i<postfixs.length;i++){
                ch=postfixs[i];
                if(ch >= '0' && ch <= '9') {
                    values.push(Integer.valueOf(String.valueOf(ch)));
                }else {
                    result=operate(ch, values.pop(),values.pop());
                    values.push(result);

                }

            }
            result=values.pop();
            if(!values.empty()){
                throw  new Exception();
            }

        } catch (NumberFormatException e) {
            System.out.println("Error in transformation!");
        }catch(EmptyStackException e){
            System.out.println("Output Error!");
        }catch(Exception e){
            System.out.println("Output Error!");
        }
        return result;
    }
    public static int operate(char op,int value1,int value2){
        int result=0;
        switch ((int) op) {
            case 43://'+'
                result=value2+value1;
                break;
            case 45://'-'
                result=value2-value1;
                break;
            case 42://'*'
                result=value2*value1;
                break;
            case 47://'/'
                result=value2/value1;
                break;
            default:
                break;
        }
        return result;
    }
    public static void main(String[] args) {
        Scanner scan=new Scanner(System.in);
        System.out.println("请输入表达式:");
        String str=scan.nextLine();
        System.out.println("后缀表达式为"+infixToPostfix(str));
        str=infixToPostfix(str);
        System.out.println("result:"+sumPostfix(str));
    }
}/*

    public void testInorder() {
        assertEquals("[1]",a.inOrder().toString());
        assertEquals("[2]",b.inOrder().toString());
        assertEquals("[1, 3, 2]",c.inOrder().toString());
        assertEquals("[4]",d.inOrder().toString());
        assertEquals("[1, 3, 2, 5, 4]",e.inOrder().toString());
    }

    public void testIsEmpty() {
        assertEquals(false,a.isEmpty());
        assertEquals(false,b.isEmpty());
        assertEquals(false,c.isEmpty());
        assertEquals(false,d.isEmpty());
        assertEquals(false,e.isEmpty());
    }


    public void testPreorder() {
        assertEquals("[1]",a.preorder().toString());
        assertEquals("[2]",b.preorder().toString());
        assertEquals("[3, 1, 2]",c.preorder().toString());
        assertEquals("[4]",d.preorder().toString());
        assertEquals("[5, 1, 3, 2, 4]",e.preorder().toString());

    }

    public void testPostorder() {
        assertEquals("[1]",a.postorder().toString());
        assertEquals("[2]",b.postorder().toString());
        assertEquals("[1, 2, 3]",c.postorder().toString());
        assertEquals("[4]",d.postorder().toString());
        assertEquals("[1, 3, 2, 4, 5]",e.postorder().toString());
    }
}
 */