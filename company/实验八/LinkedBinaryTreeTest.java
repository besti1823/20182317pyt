package 实验八;
import junit.framework.TestCase;

public class LinkedBinaryTreeTest extends TestCase {

    LinkedBinaryTree a=new LinkedBinaryTree(1);
    LinkedBinaryTree b=new LinkedBinaryTree(2);
    LinkedBinaryTree c=new LinkedBinaryTree(3,a,b);
    LinkedBinaryTree d=new LinkedBinaryTree(4);
    LinkedBinaryTree e=new LinkedBinaryTree(5,c,d);


    public void testGetRootElement() {
    }

    public void testGetLeft() {
        assertEquals(1,c.getLeft().root.element);
        assertEquals(3,e.getLeft().root.element);
    }

    public void testGetRight() {
        assertEquals(2,c.getRight().root.element);
        assertEquals(4,e.getRight().root.element);
    }

    public void testContains() {
        assertEquals(true,a.contains(1));
        assertEquals(false,a.contains(2));
        assertEquals(true,b.contains(2));
        assertEquals(false,b.contains(1));
        assertEquals(true,c.contains(1));
        assertEquals(true,c.contains(2));
        assertEquals(true,e.contains(3));
        assertEquals(true,d.contains(4));
        assertEquals(true,e.contains(5));

    }

    public void testFind() {
        assertEquals("[5, 3, 4, 1, 2]",e.levelorder().toString());
        assertEquals("[1]",a.levelorder().toString());
        assertEquals("[2]",b.levelorder().toString());
        assertEquals("[3, 1, 2]",c.levelorder().toString());
    }

    public void testSize() {
        assertEquals(1,a.size());
        assertEquals(1,b.size());
        assertEquals(3,c.size());
        assertEquals(1,d.size());
        assertEquals(5,e.size());
    }

    public void testInorder() {
        assertEquals("[1]",a.inOrder().toString());
        assertEquals("[2]",b.inOrder().toString());
        assertEquals("[1, 3, 2]",c.inOrder().toString());
        assertEquals("[4]",d.inOrder().toString());
        assertEquals("[1, 3, 2, 5, 4]",e.inOrder().toString());
    }

    public void testIsEmpty() {
        assertEquals(false,a.isEmpty());
        assertEquals(false,b.isEmpty());
        assertEquals(false,c.isEmpty());
        assertEquals(false,d.isEmpty());
        assertEquals(false,e.isEmpty());
    }


    public void testPreorder() {
        assertEquals("[1]",a.preorder().toString());
        assertEquals("[2]",b.preorder().toString());
        assertEquals("[3, 1, 2]",c.preorder().toString());
        assertEquals("[4]",d.preorder().toString());
        assertEquals("[5, 1, 3, 2, 4]",e.preorder().toString());

    }

    public void testPostorder() {
        assertEquals("[1]",a.postorder().toString());
        assertEquals("[2]",b.postorder().toString());
        assertEquals("[1, 2, 3]",c.postorder().toString());
        assertEquals("[4]",d.postorder().toString());
        assertEquals("[1, 3, 2, 4, 5]",e.postorder().toString());
    }
}
/*

    public void testInorder() {
        assertEquals("[1]",a.inOrder().toString());
        assertEquals("[2]",b.inOrder().toString());
        assertEquals("[1, 3, 2]",c.inOrder().toString());
        assertEquals("[4]",d.inOrder().toString());
        assertEquals("[1, 3, 2, 5, 4]",e.inOrder().toString());
    }

    public void testIsEmpty() {
        assertEquals(false,a.isEmpty());
        assertEquals(false,b.isEmpty());
        assertEquals(false,c.isEmpty());
        assertEquals(false,d.isEmpty());
        assertEquals(false,e.isEmpty());
    }


    public void testPreorder() {
        assertEquals("[1]",a.preorder().toString());
        assertEquals("[2]",b.preorder().toString());
        assertEquals("[3, 1, 2]",c.preorder().toString());
        assertEquals("[4]",d.preorder().toString());
        assertEquals("[5, 1, 3, 2, 4]",e.preorder().toString());

    }

    public void testPostorder() {
        assertEquals("[1]",a.postorder().toString());
        assertEquals("[2]",b.postorder().toString());
        assertEquals("[1, 2, 3]",c.postorder().toString());
        assertEquals("[4]",d.postorder().toString());
        assertEquals("[1, 3, 2, 4, 5]",e.postorder().toString());
    }
}
 */