package week10;
public class MGraph{
    int numNodes;// 顶点个数
    String[] vexs = new String[numNodes];//一维数组储存图的顶点(V)信息
    int[][] edgs = new int[numNodes][numNodes];//邻接矩阵，二维数组储存图的边或是弧(E)的信息
    class Vexs{//创建一个类型,用来表示顶点信息
        String data;//顶点的值
        Chain chain;//本顶点的链表
    }
    class Chain{//创建一个链表类型
        int id;//相邻接顶点的数组下标
        Chain next;//下一个相邻接顶点的数组下标
    }
    class ALGraph{
        int numNodes;//顶点个数
        Vexs vexs[] = new Vexs[numNodes];//顶点数组
    }
}