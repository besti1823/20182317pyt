package week10;
import java.util.Iterator;

public interface GraphADT<T>
{
    /**
     * Adds a vertex to this graph, associating object with vertex.
     * @param vertex the vertex to be added to this graph
     */
    public void addVertex(T vertex);

    /**
     * Removes a single vertex with the given value from this graph.
     * @param vertex the vertex to be removed from this graph
     */
    public void removeVertex(T vertex);

    /**
     * Inserts an edge between two vertices of this graph.
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     */
    public void addEdge(T vertex1, T vertex2);

    /**
     * Removes an edge between two vertices of this graph.
     *
     * @param vertex1 the first vertex
     * @param vertex2 the second vertex
     */
    public void removeEdge(T vertex1, T vertex2);


    public Iterator iteratorBFS(T startVertex);

    public Iterator iteratorDFS(T startVertex);


    public Iterator iteratorShortestPath(T startVertex, T targetVertex);

    public boolean isEmpty();

    public boolean isConnected();

    public int size();

    public String toString();
}
/*
   public void removeEdge(T vertex1, T vertex2);


    public Iterator iteratorBFS(T startVertex);

    public Iterator iteratorDFS(T startVertex);


    public Iterator iteratorShortestPath(T startVertex, T targetVertex);

    public boolean isEmpty();

    public boolean isConnected();

    public int size();

    public String toString();
}   public void removeEdge(T vertex1, T vertex2);


    public Iterator iteratorBFS(T startVertex);

    public Iterator iteratorDFS(T startVertex);


    public Iterator iteratorShortestPath(T startVertex, T targetVertex);

    public boolean isEmpty();

    public boolean isConnected();

    public int size();

    public String toString();
}
 */