package pyt;

import java.util.LinkedList;
import java.util.Queue;

public class erchashubianli {
    public static int i = 0;
    public static Node Build(String str)//生成一个二叉树
    {
        Node root = null;
        if ('#' != str.charAt(i))
        {
            root = new Node(str.charAt(i));
            i++;
            root.left = Build(str);
            root.Right = Build(str);
        }
        else
        {
            i++;
        }

        return root;
    }
    public static void BianLi(Node root){//遍历一个二叉树
        if(root==null)return;
        Queue<Node> q=new LinkedList<Node>();

        q.add(root);

        while(!q.isEmpty()){
            Node tree =  q.poll();
            System.out.println(tree.data);
            if(tree.left!=null)q.add(tree.left);
            if(tree.Right!=null)q.add(tree.Right);
        }
    }
}