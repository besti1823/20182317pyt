package pyt;
import java.util.*;
class Node {
    Node left;
    Node Right;
    char data;
    void print() {
        System.out.println(data + "");
    }
    public Node() {
        this.left = null;
        this.Right = null;
    }
    public Node(char data) {
        this.left = null;
        this.Right = null;
        this.data = data;
    }
}
class erchashu {
    static Node root = new Node();
    static char ch[];
    static int i = 0;
    static Node CreateTree()
    {
        Node node = null;
        if (ch[i] == '#') {
            node = null;
            i++;
        }else {
            node = new Node();
            node.data = ch[i];
            i++;
            node.left = CreateTree();
            node.Right = CreateTree();
        }
        return node;
    }
    static public void preorder(Node node)
    {
        if (node != null) {
            node.print();
            preorder(node.left);
            preorder(node.Right);
        } else {
            System.out.println("虚节点");
        }
    }
}
public class Tree {
    public static void main(String args[]) {
        Scanner reader = new Scanner(System.in);
        erchashu.ch = new char[16];
        erchashu.ch[0] = 'a';
        erchashu.ch = "AB#CD###E#F##".toCharArray();
        erchashu.root = erchashu.CreateTree();
        erchashu.preorder(erchashu.root);
    }
}
