package 实验七;
public class SearchingTest1 {
    public static void main(String[] args) {
        Searching a =new Searching();
        Integer[] tem2=new Integer[]{18, 20 ,23, 14};//创立tem2数组
        int[] tem ={18, 20 ,23, 14, 40, 43, 45, 56, 67};//创立tem数组
        System.out.println("原数组为：");
        for(int i=0;i<tem.length;i++) {
            System.out.print(tem[i] + " ");//展示原数组
        }
        System.out.println();
        //线性查找
        System.out.println("用线性查找法寻找20");
        System.out.println(a.linearSearch(tem2,0,3,20));
        //二分查找
        System.out.println("用二分查找法寻找20");
        if(a.binarySearch(tem,20)==1)
            System.out.println("找到");
        else
            System.out.println("没找到");
        //插值查找
        System.out.println("用插值查找法寻找20");
        if(a.InsertionSearch(tem,20,0,tem.length-1)==1)
            System.out.println("找到");
        else
            System.out.println("没找到");
        //斐波那契查找算法
        System.out.println("用斐波那契查找算法寻找23");
        System.out.println(a.fibonacciSearch(tem,23));
        //树表查找
        System.out.println("用树表查找法寻找23");
        System.out.println(a.binaryTreeSearch(tem2,0,tem2.length-1,23));
        //分块查找
        System.out.println("用分块查找法寻找23");
        System.out.println(a.BlockSearch(tem2,0,tem2.length-1,23));
        //以下哈希算法
        System.out.println("用哈希查找法寻找56");
        System.out.println(a.HashSearch(tem,0,tem.length-1,56));
    }
}