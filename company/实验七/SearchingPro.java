package 实验七;
import java.util.Arrays;
import java.util.HashMap;

public class SearchingPro {
    //线性查找
    public int LinearSearch(int[] data, int target) {
        int result = 0;
        int index = 0;

        while (result == 0 && index < data.length) {
            if (data[index] == target)
                return index;
            index++;
        }
        return -1;
    }

    //二分查找
    public int binarySearch(int[] data, int target) {
        int result = 0;
        int first = 0, last = data.length - 1, mid;

        while (result == 0 && first <= last) {
            mid = (first + last) / 2;  // determine midpoint
            if (data[mid] == target)
                return mid;
            else if (data[mid] > target)
                last = mid - 1;
            else
                first = mid + 1;
        }
        return -1;
    }

    //顺序查找
    public int sequenceSearch(int a[], int value) {
        for (int i = 0; i < a.length; i++)
            if (a[i] == value)
                return i;
        return -1;
    }

    //插入查找
    public int InsertionSearch(int[] a, int value, int low, int high) {
        int mid = low + (value - a[low]) / (a[high] - a[low]) * (high - low);
        if (a[mid] == value)
            return mid;
        if (a[mid] > value)
            return InsertionSearch(a, value, low, mid - 1);
        else
            return InsertionSearch(a, value, mid + 1, high);
    }


    //斐波那契查找
    public int fbSearch(int[] array, int a) {
        if (array == null || array.length == 0) {
            return -1;
        } else {
            int length = array.length;
            int[] fb = makeFbArray(length + 2);
            int k = 0;
            while (length > fb[k] - 1) {// 找出数组的长度在斐波数列（减1）中的位置，将决定如何拆分
                k++;
            }
            int[] temp = Arrays.copyOf(array, fb[k] - 1);
            for (int i = length; i < temp.length; i++) {
                if (i >= length) {
                    temp[i] = array[length - 1];
                }
            }
            int low = 0;
            int hight = array.length - 1;
            while (low <= hight) {
                int middle = low + fb[k - 1] - 1;
                if (temp[middle] > a) {
                    hight = middle - 1;
                    k = k - 1;
                } else if (temp[middle] < a) {
                    low = middle + 1;
                    k = k - 2;
                } else {
                    if (middle <= hight) {
                        return middle;// 此时mid即为查找到的位置
                    } else {
                        return hight;// 此时middle的值已经大于hight,进入扩展数组的填充部分,即最后一个数就是要查找的数。
                    }
                }
            }
            return -1;
        }
    }

    //生成一个指定长度的斐波数列
    public int[] makeFbArray(int length) {
        int[] array = null;
        if (length > 2) {
            array = new int[length];
            array[0] = 1;
            array[1] = 1;
            for (int i = 2; i < length; i++) {
                array[i] = array[i - 1] + array[i - 2];
            }
        }
        return array;
    }

    //二叉树查找算法
    public boolean binaryTreeSearch(int[] data, int key) {

        LinkedBinarySearchTree lbs = new LinkedBinarySearchTree();
        for(int i =0;i<data.length;i++)
            lbs.addElement(data[i]);


        if(lbs.find(key)!="LinkedBinaryTree")
            return true;
        else
            return false;
    }


    //分块查找法
    public int blockSearch(int[] index, int[] data, int key, int m) {
        //index是每块最大值构成的数组，st是所要查询的数组，key为所要查询的元素，m为每块的长度
        int i = LinearSearch2(index, key);
        if (i >= 0) {
            int j = i > 0 ? i * m : i;
            int len = (i + 1) * m;
            for (int k = j; k < len; k++) {
                if (key == data[k]) {
                    return k;
                }
            }
        }
        return -1;
    }

    //分块查找时先找到待找元素在哪一块
    public int LinearSearch2(int[] index, int key) {
        if (index[0] > key) {
            return 0;
        }
        for (int i = 1; i < index.length; i++) {
            if ((index[i - 1] < key) && (index[i] > key))
                return i;
        }
        return -1;
    }

    public int hashsearch(int[] data, int key) {
        HashMap<Integer, Integer> hashMap = new HashMap<>();
        for (int i = 0; i < data.length; i++)
            hashMap.put(Integer.hashCode(data[i]), i);

        int temp = Integer.hashCode(key);
        if (hashMap.containsKey(temp))
            return hashMap.get(temp);

        return -1;
    }


}