package 实验七;
import junit.framework.TestCase;
import org.junit.Test;

public class SearchingProTest extends TestCase {
    @Test
    public void testSearch() throws Exception {
        int test[] = {20, 30, 40, 50, 60, 70};
        int test1[] = {30, 50, 70};
        SearchingPro sp = new SearchingPro();
        assertEquals(0,sp.LinearSearch(test,20));//边界
        assertEquals(0,sp.binarySearch(test,20));//边界
        assertEquals(0,sp.sequenceSearch(test,20));//边界
        assertEquals(0,sp.InsertionSearch(test,20,0,4));//边界
        assertEquals(0,sp.fbSearch(test,20));//边界
        assertEquals(0,sp.blockSearch(test1,test,20,2));//边界
        assertEquals(true,sp.binaryTreeSearch(test,20));//边界
        assertEquals(0,sp.hashsearch(test,20));//边界

        assertEquals(2,sp.LinearSearch(test,40));//正常
        assertEquals(2,sp.binarySearch(test,40));//正常
        assertEquals(2,sp.sequenceSearch(test,40));//正常
        assertEquals(2,sp.InsertionSearch(test,40,0,4));//正常
        assertEquals(2,sp.fbSearch(test,40));//正常
        assertEquals(2,sp.blockSearch(test1,test,40,2));//正常
        assertEquals(true,sp.binaryTreeSearch(test,40));//正常
        assertEquals(2,sp.hashsearch(test,40));//正常

        assertEquals(-1,sp.LinearSearch(test,100));//异常
        assertEquals(-1,sp.binarySearch(test,100));//异常
        assertEquals(-1,sp.sequenceSearch(test,100));//异常
        assertEquals(-1,sp.blockSearch(test1,test,100,2));//异常
        assertEquals(-1,sp.fbSearch(test,100));//异常
        assertEquals(-1,sp.hashsearch(test,100));//异常


    }
}