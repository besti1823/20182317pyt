package 实验七;

import java.util.*;


/**
 * LinkedBinaryTree implements the BinaryTreeADT interface
 *
 * @author Lewis and Chase
 * @version 4.0
 */
public class LinkedBinaryTree<T> implements BinaryTreeADT<T>, Iterable<T>
{
    protected BinaryTreeNode<T> root;
    protected int modCount;

    public LinkedBinaryTree()
    {
        root = null;
    }

    public LinkedBinaryTree(T element)
    {
        root = new BinaryTreeNode<T>(element);
    }

    public LinkedBinaryTree(T element, LinkedBinaryTree<T> left,
                            LinkedBinaryTree<T> right)
    {
        root = new BinaryTreeNode<T>(element);
        root.setLeft(left.root);
        root.setRight(right.root);
    }
    public T getRootElement() throws EmptyCollectionException
    {
        return root.element;
    }

    public BinaryTreeNode<T> getRoot() {
        return root;
    }
    protected BinaryTreeNode<T> getRootNode() throws EmptyCollectionException
    {
        return root;
    }

    public LinkedBinaryTree<T> getLeft()
    {
        if(root == null) {
            throw new EmptyCollectionException("BinaryTree");
        }
        LinkedBinaryTree<T> result = new LinkedBinaryTree<>();
        result.root = root.getLeft();
        return result;
    }

    public LinkedBinaryTree<T> getRight()
    {
        if(root == null) {
            throw new EmptyCollectionException("BinaryTree");
        }
        LinkedBinaryTree<T> result = new LinkedBinaryTree<>();
        result.root = root.getRight();
        return result;
    }
    public boolean isEmpty()
    {
        return (root == null);
    }
    public int size()
    {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        inOrder(root, tempList);
        return tempList.size();
    }
    public int getHeight()
    {
        return height(root);
    }
    private int height(BinaryTreeNode<T> node)
    {
        int leftnumber,rightnumber;
        if(node == null)
        {
            return  0;
        }
        else
        {
            leftnumber = height(node.getLeft());
            rightnumber = height(node.getRight());
            return (leftnumber>rightnumber ? leftnumber+1 : rightnumber+1);
        }
    }
    public boolean contains(T targetElement)
    {
        BinaryTreeNode<T> current = findNode(targetElement, root);

        if (current == null)
            throw new ElementNotFoundException("LinkedBinaryTree");

        if(current.getElement()!=null)
            return true;
        else
            return false;
    }
    public T find() throws ElementNotFoundException {
        return find();
    }
    public T find(T targetElement) throws ElementNotFoundException
    {
        BinaryTreeNode<T> current = findNode(targetElement, root);

        if (current == null)
            throw new ElementNotFoundException("LinkedBinaryTree");

        return (current.getElement());
    }

    private BinaryTreeNode<T> findNode(T targetElement, BinaryTreeNode<T> next)
    {
        if (next == null)
            return null;

        if (next.getElement().equals(targetElement))
            return next;

        BinaryTreeNode<T> temp = findNode(targetElement, next.getLeft());

        if (temp == null)
            temp = findNode(targetElement, next.getRight());

        return temp;
    }
    public String toString()
    {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        inOrder(root, tempList);
        String result = "";
        for(int a=0;a<tempList.size();a++)
        {
            result += tempList.removeLast();
        }
        return result;
    }
    public Iterator<T> iterator()
    {
        return iteratorInOrder();
    }
    public Iterator<T> iteratorInOrder()
    {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        inOrder(root, tempList);

        return new TreeIterator(tempList.iterator());
    }
    protected void inOrder(BinaryTreeNode<T> node, ArrayUnorderedList<T> tempList)
    {
        if (node != null)
        {
            inOrder(node.getLeft(), tempList);
            tempList.addToRear(node.getElement());
            inOrder(node.getRight(), tempList);
        }
    }
    public Iterator<T> iteratorPreOrder()
    {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        preOrder(root, tempList);

        return new TreeIterator(tempList.iterator());
    }
    protected void preOrder(BinaryTreeNode<T> node, ArrayUnorderedList<T> tempList)
    {
        if (node != null)
        {
            tempList.addToRear(node.getElement());
            preOrder(node.getLeft(), tempList);
            preOrder(node.getRight(), tempList);
        }
    }
    public Iterator<T> iteratorPostOrder()
    {
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        postOrder(root, tempList);

        return new TreeIterator(tempList.iterator());
    }


    protected void postOrder(BinaryTreeNode<T> node, ArrayUnorderedList<T> tempList)
    {
        if (node != null)
        {
            postOrder(node.getLeft(), tempList);
            postOrder(node.getRight(), tempList);
            tempList.addToRear(node.getElement());
        }
    }

    public Iterator<T> iteratorLevelOrder()
    {
        ArrayUnorderedList<BinaryTreeNode<T>> nodes = new ArrayUnorderedList<BinaryTreeNode<T>>();
        ArrayUnorderedList<T> tempList = new ArrayUnorderedList<T>();
        BinaryTreeNode<T> current;

        nodes.addToRear(root);

        while (!nodes.isEmpty())
        {
            current = nodes.removeFirst();

            if (current != null)
            {
                tempList.addToRear(current.getElement());
                if (current.getLeft() != null)
                    nodes.addToRear(current.getLeft());
                if (current.getRight() != null)
                    nodes.addToRear(current.getRight());
            }
            else
                tempList.addToRear(null);
        }

        return new TreeIterator(tempList.iterator());
    }

    private class TreeIterator implements Iterator<T>
    {
        private int expectedModCount;
        private Iterator<T> iter;


        public TreeIterator(Iterator<T> iter)
        {
            this.iter = iter;
            expectedModCount = modCount;
        }

        public boolean hasNext() throws ConcurrentModificationException
        {
            if (!(modCount == expectedModCount))
                throw new ConcurrentModificationException();

            return (iter.hasNext());
        }


        public T next() throws NoSuchElementException
        {
            if (hasNext())
                return (iter.next());
            else
                throw new NoSuchElementException();
        }

        public void remove()
        {
            throw new UnsupportedOperationException();
        }
        public void removeRightSubtree(T targetelement)
        {
            BinaryTreeNode node = findNode(targetelement,root);
            if(node.right == null)
                System.out.println("右子树为空，删除操作没有作用，请查对");
            else
                node.right = null;
        }
        public void removeAllElements()
        {
            root = null;
        }
    }
    public static int count;
    public int CountLeaf(BinaryTreeNode tree){
        if(tree == null)
            count = 0;
        else {
            if(tree.getLeft()==null && tree.getRight() == null)
                count ++;
            if (tree.getLeft()!=null||tree.getRight()!=null)
            {
                CountLeaf(tree.getLeft());
                CountLeaf(tree.getRight());
            }
        }
        return count;
    }
    //方法一：使用递归实现层次遍历，并按照层次顺序输出每个节点内容.
    public void LevelPrint1()
    {
        int height = getHeight();
        for(int i = 1; i <= height; i++){
            levelOrdertraversal(root,i);
        }
    }
    //被递归调用的方法。
    private void levelOrdertraversal(BinaryTreeNode node, int level){
        if(node == null || level < 1)
        {
            return;
        }
        if(level == 1)
        {
            System.out.print(node.getElement() + "\n");
            return ;
        }
        levelOrdertraversal(node.getLeft(),level - 1);
        levelOrdertraversal(node.getRight(),level - 1);
    }
    //方法二：非递归实现层次遍历，并按照层次顺序输出每个节点内容.
    public void LevelPrint2()
    {
        BinaryTreeNode temp;
        //使用队列存储二叉树
        Queue<BinaryTreeNode> queue = new LinkedList<BinaryTreeNode>();
        queue.offer(root);//根结点入队；
        while(!queue.isEmpty()){
            temp=queue.poll();//从队头取元素
            System.out.print(temp.getElement()+"\n");
            //若该元素有左孩子，则左孩子入队；
            if(temp.getLeft()!=null)
                queue.offer(temp.getLeft());
            //若该元素有右孩子，则右孩子入队；
            if(temp.getRight()!=null){
                queue.offer(temp.getRight());
            }
        }
    }

}