package 实验七;
public class ElementNotFoundException extends RuntimeException
{

    public ElementNotFoundException(String collection)
    {
        super("The target element is not in this " + collection);
    }
}