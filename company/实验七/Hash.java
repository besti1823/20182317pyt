package 实验七;
public class Hash {
    public static int count = 0;//冲突次数
    int times;//初始的查找次数
    public int num = 11;//添加的元素个数
    String result;
    public LinearNode[] list ;

    public Hash()
    {
        list = new LinearNode[num];
    }

    public void ReceiveNum(int number)
    {
        int index = number%11;
        LinearNode linearNode = new LinearNode(number);
        if (list[index]==null)
            list[index] = linearNode;
        else
        {
            LinearNode current = list[index];
            while (!(current.getNext() ==null))
            {
                current = current.getNext();
                count++;
                times++;
            }
            current.setNext(linearNode);
            count++;
            times++;
        }
        times++;

    }

    public String toString()
    {
        result = "平均查找次数ASL："+(times+count)+"/"+num+"\n"+
                "冲突次数为："+ count;
        System.out.println(result);
        return result;

    }

}