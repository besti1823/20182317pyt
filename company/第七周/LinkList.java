package pp10;

import java.io.*;
import java.util.Scanner;

public class LinkList {
    public static void main(String[] args) throws IOException
    {
        // 创建链表
        Scanner scan = new Scanner(System.in);
        Means fangfa = new Means();
        String num = " ";
        int  a = 0, number;
        int[] ints;
        System.out.println("输入数字（输入stop时停止输入，进行下一项操作）:");

        while(!num.equals("stop"))
        {
            num = scan.next();
            if (num.equals("stop"))
                continue;
            fangfa.add((Integer.parseInt(num)));
        }

        // 实现节点插入、删除、输出操作
        System.out.println("Before insert ："+fangfa);
        fangfa.insert(7,1813);
        fangfa.insert(5,9527);
        fangfa.insert(2,2018);
        fangfa.insert(4,2317);
        System.out.println("The insert result is:"+fangfa);
        System.out.println("删除操作:");
        System.out.println("输入1个想要删除的数字");
        fangfa.delete(scan.nextInt());
        System.out.println("The delete result is:"+fangfa);


        System.out.println("输出：");
        System.out.println(fangfa.toString());

        //使用冒泡排序法或者选择排序法根据数值大小对链表进行排序
        fangfa.sort();
        System.out.println("排序结果：\n "+fangfa);
    }
}