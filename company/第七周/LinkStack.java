package pp10;
public class LinkStack<T> implements StackADT<T>{

    private int count;
    private Node<T> top;

    public LinkStack() {
        count=0;
        top=null;
    }

    public void push(T t) {
        Node<T> newNode=new  Node<T>(t);
        newNode.setNext(top);
        count++;
        top=newNode;
    }


    public T pop() throws EmptyCollectionException {
        if(isEmpty()){
            throw new EmptyCollectionException("Stack");
        }
        T pop=((Node<T>) top).getElement();
        top=top.getNext();
        count--;
        return pop;
    }


    public T peek() throws EmptyCollectionException {
        if(isEmpty()){
            throw new EmptyCollectionException("Stack");
        }
        return ((Node<T>) top).getElement();
    }


    public boolean isEmpty() {
        if(size()==0){
            return true;
        }
        return false;
    }


    public int size() {
        return count;
    }

}
