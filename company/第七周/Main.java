package pp10;
public class Main {
    public static void main(String[] args) {
        StackADT<String> stack = new LinkStack<String>();
        stack.push("5");
        stack.push("3");
        stack.push("4");
        while (!stack.isEmpty()) {
            System.out.println("栈的长度为" + stack.size());
            try {
                System.out.println("弹出栈顶元素：" + stack.peek());
                System.out.println("弹栈：" + stack.pop());
            } catch (EmptyCollectionException e) {
                e.printStackTrace();
            }
        }
    }
}
