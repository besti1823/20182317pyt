public class CircularArrayQueue<T> implements Queue<T> {
    private final int DEFAULT_CAPACITY = 10;
    private int front,rear,count;
    private T[] queue;
    private boolean flag = true;

    public CircularArrayQueue(){
        front = rear = count = 0;
        queue = (T[])(new Object[DEFAULT_CAPACITY]);
    }

    @Override
    public void enqueue(T element) {
        if(count == queue.length)
            expandCapacity();

        queue[rear] = element;
        rear = (rear+1) % queue.length;
        count++;
        flag = false;
    }

    private void expandCapacity() {
        T[] larger = (T[])(new Object[DEFAULT_CAPACITY*2]);

        for(int index = 0;index < count;index++)
            larger[index] = queue[(front+index) % queue.length];

        front = 0;
        rear = count;
        queue =larger;
    }

    @Override
    public T dequeue() {
        int temp = front;
        front = (front+1) % queue.length;
        count--;
        flag = true;
        return queue[temp];
    }

    @Override
    public T first() {
        return queue[front];
    }

    @Override
    public boolean isEmpty() {
        if(front != rear)return false;
        else return flag;
    }

    @Override
    public int size() {
        if(front != rear)return (rear+queue.length-front) % queue.length;
        else if(flag == false)return queue.length;
        else return 0;
    }

    @Override
    public String toString() {
        String rpy = "";
        int temp;
        for(temp = 0;temp < this.size();temp++){
            rpy = rpy+" "+queue[front+temp];
        }
        return rpy;
    }
}