public interface Queue<T> {
    public void enqueue(T element);//尾部插入元素
    public T dequeue();//头部删除元素
    public T first();//查看头部元素
    public boolean isEmpty();//如果空返回真
    public int size();//返回元素个数
    public String toString();//返回描述字符串
}