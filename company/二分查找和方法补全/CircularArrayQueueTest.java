import javax.security.auth.login.CredentialException;

public class CircularArrayQueueTest {
    public static void main(String[] args) {
        CircularArrayQueue caq = new CircularArrayQueue();

        System.out.println(caq.toString());
        System.out.println("Empty? "+caq.isEmpty());

        caq.enqueue(5);
        caq.enqueue(9);
        caq.enqueue(13);
        caq.enqueue(14);

        System.out.println(caq.toString());

        caq.enqueue(20);
        caq.enqueue(26);
        caq.enqueue(37);
        caq.enqueue(39);
        caq.enqueue(41);
        caq.enqueue(51);

        System.out.println(caq.toString());
        System.out.println("Empty? "+caq.isEmpty());

        caq.dequeue();
        caq.dequeue();
        caq.dequeue();
        caq.dequeue();

        System.out.println(caq.toString());

        caq.dequeue();
        caq.dequeue();
        caq.dequeue();
        caq.dequeue();
        caq.dequeue();
        caq.dequeue();

        System.out.println(caq.toString());
        System.out.println("Empty? "+caq.isEmpty());
    }
}