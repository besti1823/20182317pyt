package 实验六;


import java.io.*;
import java.util.Scanner;
import java.util.StringTokenizer;

/*磁盘读取一个文件， 这个文件有两个数字。
	从文件中读入数字1，  插入到链表第 5 位，并打印所有数字，和元素的总数。 保留这个链表，继续下面的操作。
	从文件中读入数字2， 插入到链表第 0 位，并打印所有数字，和元素的总数。 保留这个链表，并继续下面的操作。
	从链表中删除刚才的数字1.  并打印所有数字和元素的总数。*/

public class exp2 {
    public static void main(String[] args) throws IOException {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入你的学号（依次取出的两位数）、日期和时间\n");
        String input =scan.nextLine();
        int[] number = new int[100];
        String[] numbers = input.split(" ");
        for(int a = 0;a<numbers.length;a++)
        {
            number[a] = Integer.parseInt(numbers[a]);
        }
        FirstLinkedlist Head = new FirstLinkedlist(number[0]);
        int nPengyantai = 1;
        for(int a=1; a < numbers.length;a++)
        {
            FirstLinkedlist node = new FirstLinkedlist(number[a]);
            InsertNode(Head,node);
            nPengyantai++;
        }
        //打印链表元素。
        System.out.println("链表元素是:");
        PrintLinkedlist(Head);
        System.out.println();
        System.out.println("元素总数为:" + nPengyantai);

        //磁盘读取一个文件， 这个文件有两个数字
        File file = new File("C:\\Users\\12156\\IdeaProjects\\HelloJDB\\src\\实验六", "num.txt");
        if (!file.exists()) {
            file.createNewFile();
        }
        Reader reader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader);
        StringTokenizer stringTokenizer = new StringTokenizer(bufferedReader.readLine());
        int  num = stringTokenizer.countTokens();
        int[] counts = new int[num];
        int a=0;
        while(stringTokenizer.hasMoreTokens()){
            counts[a]=Integer.parseInt(stringTokenizer.nextToken());
            a++;
        }
        int firstnum =counts[0];//从文件中读取的第一个数
        int secondnum = counts[1];//从文件中读取的第二个数




    //链表的输出方法。
    public static void PrintLinkedlist(FirstLinkedlist Head) {
        FirstLinkedlist node = Head;//链表的头不能动。
        while (node != null) {
            System.out.print(  node.number );
            node = node.next;
            if(node!=null)
                System.out.print(",");
            else
                System.out.print(" ");

        }
    }








}