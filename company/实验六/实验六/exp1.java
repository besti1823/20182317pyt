package 实验六;

import java.util.*;
import java.io.*;

public class exp1 {
    public static  void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("请输入你的学号（依次取出的两位数）、日期和时间" );
        String input = scan.nextLine();
        int[] number = new int[200];
        String[] numbers = input.split(" ");
        for(int a = 0;a<numbers.length;a++)
        {
            number[a] = Integer.parseInt(numbers[a]);
        }
        FirstLinkedlist Head = new FirstLinkedlist(number[0]);
        int nPengyantai = 1;
        for(int a=1; a < numbers.length;a++)
        {
            FirstLinkedlist node = new FirstLinkedlist(number[a]);
            InsertNode(Head,node);
            nPengyantai++;
        }
        //打印链表元素。
        System.out.println("链表元素是:");
        PrintLinkedlist(Head);
        System.out.println();
        System.out.println("元素总数为:" + nPengyantai);

    }
    //链表的输出方法。
    public static void PrintLinkedlist(FirstLinkedlist Head) {
        FirstLinkedlist node = Head;//链表的头不能动。
        while (node != null) {
            System.out.print(  node.number );
            node = node.next;
            if(node!=null)
                System.out.print(",");
            else
                System.out.print(" ");

        }
    }
    //尾插法的方法。
    public static void InsertNode(FirstLinkedlist Head, FirstLinkedlist node) {
        FirstLinkedlist temp = Head;
        //遍历链表，找到链表末尾。
        while (temp.next != null) {
            temp = temp.next;
        }
        temp.next = node;
    }



}