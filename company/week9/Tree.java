package week9;

import java.util.Scanner;

public class Tree
{
    private LinkedBinaryTree<String> tree;

    //-----------------------------------------------------------------
    //  Sets up the diagnosis question tree.
    //-----------------------------------------------------------------
    public Tree()
    {
        String e1 = "天气如何";
        String e2 = "温度如何";
        String e3 = "";
        String e4 = "";
        String e5 = "是否有约";
        String e6 = "";
        String e7 = "";
        String e8 = "";
        String e9 = "";
        String e10 = "出去玩吧";
        String e11 = "";
        String e12 = "";
        String e13 = "";

        LinkedBinaryTree<String> n2, n3, n4, n5, n6, n7, n8, n9,
                n10, n11, n12, n13;

        n8 = new LinkedBinaryTree<String>(e8);
        n9 = new LinkedBinaryTree<String>(e9);
        n4 = new LinkedBinaryTree<String>(e4, n8, n9);

        n10 = new LinkedBinaryTree<String>(e10);
        n11 = new LinkedBinaryTree<String>(e11);
        n5 = new LinkedBinaryTree<String>(e5, n10, n11);

        n12 = new LinkedBinaryTree<String>(e12);
        n13 = new LinkedBinaryTree<String>(e13);
        n6 = new LinkedBinaryTree<String>(e6, n12, n13);

        n7 = new LinkedBinaryTree<String>(e7);

        n2 = new LinkedBinaryTree<String>(e2, n4, n5);
        n3 = new LinkedBinaryTree<String>(e3, n6, n7);

        tree = new LinkedBinaryTree<String>(e1, n2, n3);
    }

    public void diagnose()
    {
        Scanner scan = new Scanner(System.in);
        LinkedBinaryTree<String> current = tree;
        System.out.println ("So, I will ask you some questions.");
        while (current.size() > 1)
        {
            System.out.println (current.getRootElement());
            if (scan.nextLine().equals("y"))
                current = current.getLeft();
            else
                current = current.getRight();
        }

        System.out.println (current.getRootElement());
    }
}
/*
import java.util.Scanner;

public class shu
{
    private LinkedBinaryTree<String> tree;

    //-----------------------------------------------------------------
    //  Sets up the diagnosis question tree.
    //-----------------------------------------------------------------
    public shu()
    {
        String e1 = "他蔡徐吗？";
        String e2 = "他是不是个偶像？";
        String e3 = "";
        String e4 = "";
        String e5 = "他会唱跳，Rap，篮球吗？";
        String e6 = "";
        String e7 = "";
        String e8 = "";
        String e9 = "";
        String e10 = "Basketball!";
        String e11 = "";
        String e12 = "";
        String e13 = "";

        LinkedBinaryTree<String> n2, n3, n4, n5, n6, n7, n8, n9,
                n10, n11, n12, n13;

        n8 = new LinkedBinaryTree<String>(e8);
        n9 = new LinkedBinaryTree<String>(e9);
        n4 = new LinkedBinaryTree<String>(e4, n8, n9);

        n10 = new LinkedBinaryTree<String>(e10);
        n11 = new LinkedBinaryTree<String>(e11);
        n5 = new LinkedBinaryTree<String>(e5, n10, n11);

        n12 = new LinkedBinaryTree<String>(e12);
        n13 = new LinkedBinaryTree<String>(e13);
        n6 = new LinkedBinaryTree<String>(e6, n12, n13);

        n7 = new LinkedBinaryTree<String>(e7);

        n2 = new LinkedBinaryTree<String>(e2, n4, n5);
        n3 = new LinkedBinaryTree<String>(e3, n6, n7);

        tree = new LinkedBinaryTree<String>(e1, n2, n3);
    }

    public void diagnose()
    {
        Scanner scan = new Scanner(System.in);
        LinkedBinaryTree<String> current = tree;
        System.out.println ("So, I will ask you some questions.");
        while (current.size() > 1)
        {
            System.out.println (current.getRootElement());
            if (scan.nextLine().equals("y"))
                current = current.getLeft();
            else
                current = current.getRight();
        }

        System.out.println (current.getRootElement());
    }
}
 */