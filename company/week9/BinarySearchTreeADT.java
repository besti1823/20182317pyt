package week9;
public interface BinarySearchTreeADT<T> extends BinaryTreeADT<T>
{
    /**
     * Adds the specified element to the proper location in this tree.
     *
     */
    public void addElement(T element);

    /**
     * Removes and returns the specified element from this tree.
     *
     */
    public T removeElement(T targetElement);

    /**
     * Removes all occurences of the specified element from this tree.

     */
    public void removeAllOccurrences(T targetElement);

    /**
     * @return the smallest element from the tree.
     */
    public T removeMin();

    /**
     *
     * @return the largest element from the tree
     */
    public T removeMax();

    /**
     * Returns the smallest element in this tree without removing it.

     */
    public T findMin();

    /**
     *
     * @return the largest element in the tree
     */
    public T findMax();
}