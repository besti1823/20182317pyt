package 第六周;
import java.io.*;

public class FileTest2 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("Test2.txt");
        //File file = new File("test2.txt");
//        File file1 = new File("C:\Program Files\Java\jdk-13\bin\java.exe");
//        file1.mkdir();
//        file1.mkdirs();
        if (!file.exists()) {
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] test2 = {'T','e','s','t','2'};
        byte[] buffer = new byte[6];
        outputStream1.write(test2);
        String content = "";
        int flag = 0;
        InputStream inputStream2 = new FileInputStream(file);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream2);

        while ((flag =bufferedInputStream.read(buffer))!=-1){
            content += new String(buffer,0,flag);
        }

        System.out.println(content);
        bufferedInputStream.close();
    }
}