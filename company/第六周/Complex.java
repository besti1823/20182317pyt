package 第六周;
public class Complex {
    private double RealPart;
    private double ImagePart;

    public Complex(double R,double I){
        this.RealPart=R;
        this.ImagePart=I;//输入实部和虚部
    }

    public void setRealPart(double realPart) {
        RealPart = realPart;
    }

    public void setImagePart(double imagePart) {
        ImagePart = imagePart;
    }

    public double getRealPart() {
        return RealPart;
    }

    public double getImagePart() {
        return ImagePart;
    }

    public Complex complexAdd(Complex a) {//加法
        return new Complex(this.getRealPart()+a.getRealPart(),this.getImagePart()+a.getImagePart());
    }

    public Complex complexSub(Complex a) {//减法
        return new Complex(this.getRealPart()-a.getRealPart(),this.getImagePart()-a.getImagePart());
    }

    public Complex complexMulti(Complex a) {//乘法
        return new Complex(this.getRealPart()*a.getRealPart()-this.getImagePart()*a.getImagePart(),this.getRealPart()*a.getImagePart()+this.getImagePart()*a.getRealPart());
    }

    public Complex complexDiv(Complex a) {//除法
        double d=Math.sqrt(a.getRealPart()*a.getRealPart())+Math.sqrt(a.getImagePart()*a.getImagePart());
        double e=this.getImagePart()*a.getRealPart()-this.getRealPart()*a.getImagePart();
        double f=this.getRealPart()*a.getRealPart()+this.getImagePart()*a.getImagePart();
        return new Complex(f/d, e/d);
    }

    public String toString() {
        if(this.getImagePart()<0)
            return RealPart + "" + ImagePart + "i";
        else if(this.getImagePart()==0)
            return "" + RealPart;
        else
            return RealPart + "+" +ImagePart + "i";
    }
}