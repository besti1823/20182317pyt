package pp10;
//pyt   2019/10/15
//第10章作业
public class StringTooLongException extends Exception{
    public StringTooLongException(String s) {
        super(s);
    }
}