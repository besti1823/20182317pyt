package 第六周;
import java.io.*;

public class FileTest1 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("Test1.txt");
        //File file = new File("test1.txt");
//        File file1 = new File("C:\Program Files\Java\jdk-13\bin\java.exe");
//        file1.mkdir();
//        file1.mkdirs();
        if (!file.exists()){
            file.createNewFile();
        }
        // file.delete();
        //（2）文件读写
        //第一种：字节流读写，先写后读
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] test1 = {'T','e','s','t','1'};
        outputStream1.write(test1);//将 b.length 个字节从指定的 byte 数组写入此输出流。

        InputStream inputStream1 = new FileInputStream(file);
        while (inputStream1.available()> 0){
            System.out.print((char) inputStream1.read()+"  ");
        }
        inputStream1.close();
        System.out.println("\n文件读写结束：OutputStream、InputStream先写后读");
    }
}