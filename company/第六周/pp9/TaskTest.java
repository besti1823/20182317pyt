package pp9;
//第九章作业
import java.util.*;
//pyt   2019/10/15
public class TaskTest {
    public static void main(String[] args) {
        Task[] list=new Task[4];
        int[] a=new int[4];
        List<Task> tasklist = new ArrayList<>();

        Task task=new Task("");
        list[0]=new Task("blue");
        list[1]=new Task("red");
        list[2]=new Task("yellow");
        list[3]=new Task("green");

        for(int i=0;i<4;i++){
            tasklist.add(list[i]);
        }

        Scanner in=new Scanner(System.in);
        System.out.println("请输入一组优先级：(数字越大优先级越高）");
        for(int i=0;i<4;i++){
            a[i]=in.nextInt();
        }

        task.set(list,a);
        for(Task c:list){
            System.out.println(c.getName() + ",优先级:" + c.getPriority());
        }

        Arrays.sort(list);
        System.out.println("优先级排序后：");
        for(Task c:list){
            System.out.println(c.getName()+",优先级:"+c.getPriority());
        }

    }
}