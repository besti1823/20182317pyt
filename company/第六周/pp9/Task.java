package pp9;
//第九章作业
//pyt   2019/10/15
public class Task implements Priority,Comparable<Task> {
    private int priority;
    private String name;

    public Task(String eName) {
        name = eName;
    }

    @Override
    public void setPriority(int n) {
        priority=n;
    }

    @Override
    public int getPriority() {
        return priority;
    }

    public void set(Task[] t,int[] n)
    {
        for(int i = 0; i < t.length;i++)
        {
            t[i].setPriority(n[i]);
        }
    }

    public String getName()
    {
        return name;
    }

    @Override
    public int compareTo(Task c)
    {
        if(priority > c.priority)
            return 1;
        else if(priority<c.priority)
            return -1;
        else
            return 0;
    }

}