package pp9;
//第九章作业
//pyt   2019/10/15
abstract public class StaffMember {
    protected String name;
    protected String address;
    protected String phone;
    protected int rank;

    public StaffMember(String eName, String eAddress, String ePhone) {
        name = eName;
        address = eAddress;
        phone = ePhone;
    }

    @Override
    public String toString() {
        String a = "name: "+name+"\n";
        a += "address:"+address+"\n";
        a += "" + "phone:"+phone+"\n";
        return a;
    }

    public abstract double pay();
    public abstract int houliday();
}