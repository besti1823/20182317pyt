package pp9;
//第九章作业
//pyt   2019/10/15
public class Employee extends  StaffMember{
    protected String socialSecurityNumber;
    protected double payRate;
    protected int rank;

    public Employee(String eName, String eAddress, String ePhone, String socSecNumber, double rate) {
        super(eName, eAddress, ePhone);
        socialSecurityNumber = socSecNumber;
        payRate = rate;
        rank=2;
    }

    @Override
    public String toString() {
        String result = super.toString();
        result += "\nSocial Security Number:"+socialSecurityNumber;
        return result;
    }

    public double pay(){
        return payRate;
    }

    public int houliday(){
        return rank;
    }
}