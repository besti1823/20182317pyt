package pp9;//第九章作业
//pyt   2019/10/15
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public interface Encryptable {
    public String encrypt() throws UnsupportedEncodingException, IOException, ClassNotFoundException;
    public String decrypt() throws IOException, ClassNotFoundException;
}