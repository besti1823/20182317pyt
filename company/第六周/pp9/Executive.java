package pp9;//第九章作业
//pyt   2019/10/15
public class Executive extends Employee{
    private double bonus;
    public Executive(String eName, String eAddress, String ePhone, String socSecNumber, double rate)
    {
        super(eName, eAddress, ePhone, socSecNumber, rate);
        bonus = 0;
        rank = 3;
    }



    public void awardBonus(double execBonus){//set奖金数额
        bonus = execBonus;
    }

    public double pay(){
        double payment = super.pay()+bonus;
        bonus = 0;
        return payment;
    }

    public int houliday(){
        return rank;
    }
}