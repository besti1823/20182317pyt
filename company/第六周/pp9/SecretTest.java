package pp9;//第九章作业
//pyt   2019/10/15
import java.io.IOException;
public class SecretTest {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        Encryptable hush ;

        Secret a=new Secret("Java is very difficult!");
        Password b=new Password("zhiqiang zui shuai");

        hush=a;
        System.out.println(hush);
        hush.encrypt();
        System.out.println(hush);
        hush.decrypt();
        System.out.println(hush);

        hush=b;
        System.out.println(hush);
        hush.encrypt();
        System.out.println(hush);
        hush.decrypt();
        System.out.println(hush);
    }
}