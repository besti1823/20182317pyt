package 第六周;
import java.io.*;

public class FileTest4 {
    public static void main(String[] args) throws IOException {
        //（1）文件创建（文件类实例化）
        File file = new File("Test4.txt");
        //File file = new File("test3.txt");
//        File file1 = new File("C:\Program Files\Java\jdk-13\bin\java.exe");
//        file1.mkdir();
//        file1.mkdirs();
        if (!file.exists()){
            file.createNewFile();
        }
        OutputStream outputStream1 = new FileOutputStream(file);
        byte[] test2 = {'T','e','s','t','4'};
        byte[] buffer = new byte[6];
        outputStream1.write(test2);
        String content = "";

        Reader reader2 = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(reader2);
        System.out.println("\n下面是用BufferedReader读出的数据：");
        while ((content =bufferedReader.readLine())!= null){
            System.out.println(content);
        }
    }
}