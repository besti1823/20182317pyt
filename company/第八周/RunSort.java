package week8;
import java.io.OutputStream;
import java.util.List;

public class RunSort {
    private static int countS=0;
    private static int countI=0;
    private static int countB=0;
    private static int countQ=0;
    private static int countM=0;
    private static long TimeS;
    private static long TimeI;
    private static long TimeB;
    private static long TimeQ;
    private static long TimeM;
    public static void main(String[] args) {
        int List1[]={1,4,7,8,9,11,15,26,45,97};
        int List2[]={5,2,14,13,45,33,65,34,35,9};
        int List3[]={68,55,45,32,23,22,13,6,1,0};
        System.out.println("SelectionSort");
        System.out.println("List1:"+SelectionSort(List1));
        System.out.println("List2:"+SelectionSort(List2));
        System.out.println("List3:"+SelectionSort(List3));

        int List4[]={1,4,7,8,9,11,15,26,45,97};
        int List5[]={5,2,14,13,45,33,65,34,35,9};
        int List6[]={68,55,45,32,23,22,13,6,1,0};
        System.out.println("InsertSort:");
        System.out.println("List1:"+InsertSort(List4));
        System.out.println("List2:"+InsertSort(List5));
        System.out.println("List3:"+InsertSort(List6));

        int List7[]={1,4,7,8,9,11,15,26,45,97};
        int List8[]={5,2,14,13,45,33,65,34,35,9};
        int List9[]={68,55,45,32,23,22,13,6,1,0};
        System.out.println("BubbleSort:");
        System.out.println("List1:"+BubbleSort(List7));
        System.out.println("List2:"+BubbleSort(List8));
        System.out.println("List3:"+BubbleSort(List9));

        int List10[]={1,4,7,8,9,11,15,26,45,97};
        int List11[]={5,2,14,13,45,33,65,34,35,9};
        int List12[]={68,55,45,32,23,22,13,6,1,0};
        System.out.println("QuickSort:");
        System.out.print("List1:");
        QuickSort(List10,0,List10.length-1);
        System.out.println(OutputList(List10)+";CompareTimes:"+countQ+";TimeUsage:"+TimeQ+"ns;");
        System.out.print("List2:");
        QuickSort(List11,0,List11.length-1);
        System.out.println(OutputList(List11)+";CompareTimes:"+countQ+";TimeUsage:"+TimeQ+"ns;");
        System.out.print("List3:");
        QuickSort(List12,0,List12.length-1);
        System.out.println(OutputList(List12)+";CompareTimes:"+countQ+";TimeUsage:"+TimeQ+"ns;");

        int List13[]={1,4,7,8,9,11,15,26,45,97};
        int List14[]={5,2,14,13,45,33,65,34,35,9};
        int List15[]={68,55,45,32,23,22,13,6,1,0};
        System.out.println("MergeSort:");
        System.out.print("List1:");
        sort(List13,0,List13.length-1);
        System.out.println(OutputList(List13)+";CompareTimes:"+countM+";TimeUsage:"+TimeM+"ns;");
        System.out.print("List2:");
        sort(List14,0,List14.length-1);
        System.out.println(OutputList(List14)+";CompareTimes:"+countM+";TimeUsage:"+TimeM+"ns;");
        System.out.print("List3:");
        sort(List15,0,List15.length-1);
        System.out.println(OutputList(List15)+";CompareTimes:"+countM+";TimeUsage:"+TimeM+"ns;");
    }

    public static String OutputList(int temp[]){
        String string="";
        for (int i=0;i<temp.length;i++){
            string=string+temp[i]+" ";
        }
        return "List:{"+string+"}";
    }

    public static String SelectionSort(int List[]){
        int tp;
        long startTime=System.nanoTime();
        for (int i=0;i<List.length-1;i++){
            int temp=i;
            for (int j=i+1;j<List.length;j++){
                if (List[i]>List[j]){
                    if (List[temp]>List[j]){
                        temp=j;
                    }
                    countS++;
                }
                countS++;
            }
            tp=List[i];
            List[i]=List[temp];
            List[temp]=tp;
        }
        long endTime=System.nanoTime();
        TimeS=endTime-startTime;

        String str="";
        for (int i=0;i<List.length;i++){
            str=str+List[i]+" ";
        }

        String result="List:{"+str+"};CompareTimes:"+countS+";TimeUsage:"+TimeS+"ns;";
        return result;
    }

    public static String InsertSort(int List[]){
        int temp;
        int left;
        long startTime=System.nanoTime();
        for (int i=1;i<List.length-1;i++){
            left=i-1;
            temp=List[i];
            countI++;
            while (left>=0&&List[left]>List[i]){
                List[i]=List[left];
                left--;
            }
            List[left+1]=temp;
        }
        long endTime=System.nanoTime();
        TimeI=endTime-startTime;
        String str="";
        for (int i=0;i<List.length;i++){
            str=str+List[i]+" ";
        }
        String result="List:{"+str+"};CompareTimes:"+countI+";TimeUsage:"+TimeI+"ns;";
        return result;
    }

    public static String BubbleSort(int List[]){
        long startTime=System.nanoTime();
        for (int i=1;i<List.length;i++){
            for (int j=1;j<List.length-i;j++){
                countB++;
                if (List[j]>List[j+1]){
                    int temp=List[j];
                    List[j]=List[j+1];
                    List[j+1]=temp;
                }
            }
        }
        long endTime=System.nanoTime();
        TimeB=endTime-startTime;
        String str="";
        for (int i=0;i<List.length;i++){
            str=str+List[i]+" ";
        }
        String result="List:{"+str+"};CompareTimes:"+countB+";TimeUsage:"+TimeB+"ns;";
        return result;
    }
/*
public static String LinearSearch(Contact searchElement){
        boolean searchSuccess=false;
        String result="";
        for (int i=0;i<players.length;i++){
            if (players[i].compareTo(searchElement)==0){
                result="Found!At locate ["+i+"]";
                searchSuccess=true;
                break;
            }
        }
        if (searchSuccess==false){
            result="Not Found!";
        }
        return result;
    }

    public static String BinarySearch(Contact searchElement){
        //排序
        for (int i=0;i<players.length-1;i++){
            for (int j=i+1;j<players.length;j++){
                if (players[i].compareTo(players[j])>0){
                    Contact temp=players[i];
                    players[i]=players[j];
                    players[j]=temp;
                }
            }
        }

        //查找
        int high,low,middle;
        low=0;
        high=players.length-1;
        middle=(low+high)/2;
        boolean searchSuccess=false;
        String result="";

        while (searchSuccess==false){
            if (low>high){
                searchSuccess=false;
                result="Not Found!";
                break;
            }
            else {
                if (players[middle].compareTo(searchElement)==0){
                    searchSuccess=true;
                    result="Found!At locate ["+middle+"]";
                    break;
                }
                else if (searchElement.compareTo(players[middle])<0){
                    high=middle-1;
                }
                else if (searchElement.compareTo(players[middle])>0){
                    low=middle+1;
                }
                middle=(low+high)/2;
            }
        }public static String LinearSearch(Contact searchElement){
        boolean searchSuccess=false;
        String result="";
        for (int i=0;i<players.length;i++){
            if (players[i].compareTo(searchElement)==0){
                result="Found!At locate ["+i+"]";
                searchSuccess=true;
                break;
            }
        }
        if (searchSuccess==false){
            result="Not Found!";
        }
        return result;
    }

    public static String BinarySearch(Contact searchElement){
        //排序
        for (int i=0;i<players.length-1;i++){
            for (int j=i+1;j<players.length;j++){
                if (players[i].compareTo(players[j])>0){
                    Contact temp=players[i];
                    players[i]=players[j];
                    players[j]=temp;
                }
            }
        }

        //查找
        int high,low,middle;
        low=0;
        high=players.length-1;
        middle=(low+high)/2;
        boolean searchSuccess=false;
        String result="";

        while (searchSuccess==false){
            if (low>high){
                searchSuccess=false;
                result="Not Found!";
                break;
            }
            else {
                if (players[middle].compareTo(searchElement)==0){
                    searchSuccess=true;
                    result="Found!At locate ["+middle+"]";
                    break;
                }
                else if (searchElement.compareTo(players[middle])<0){
                    high=middle-1;
                }
                else if (searchElement.compareTo(players[middle])>0){
                    low=middle+1;
                }
                middle=(low+high)/2;
            }
        }
*/

    public static void QuickSort(int List[],int left,int right){
        if(left>right)
            return;
        long startTime=System.nanoTime();
        int pivot=List[left];//定义基准值为数组第一个数
        int i=left;
        int j=right;

        while(i<j)
        {
            countQ++;
            while(pivot<=List[j]&&i<j)//从右往左找比基准值小的数
                j--;
            while(pivot>=List[i]&&i<j)//从左往右找比基准值大的数
                i++;
            if(i<j)                     //如果i<j，交换它们
            {
                int temp=List[i];
                List[i]=List[j];
                List[j]=temp;
            }
        }
        List[left]=List[i];
        List[i]=pivot;//把基准值放到合适的位置
        QuickSort(List,left,i-1);//对左边的子数组进行快速排序
        QuickSort(List,i+1,right);//对右边的子数组进行快速排序
        long endTime=System.nanoTime();
        TimeQ=endTime-startTime;
    }

    public static void sort(int[] arr, int L, int R) {
        long startTime=System.nanoTime();
        if(L == R) {
            return;
        }
        int mid = L + ((R - L) >> 1);
        sort(arr, L, mid);
        sort(arr, mid + 1, R);
        merge(arr, L, mid, R);
        long endTime=System.nanoTime();
        TimeM=endTime-startTime;
    }
    public static void merge(int[] arr, int L, int mid, int R) {
        int[] temp = new int[R - L + 1];
        int i = 0;
        int p1 = L;
        int p2 = mid + 1;
        // 比较左右两部分的元素，哪个小，把那个元素填入temp中
        while(p1 <= mid && p2 <= R) {
            countM++;
            temp[i++] = arr[p1] < arr[p2] ? arr[p1++] : arr[p2++];
        }
        // 上面的循环退出后，把剩余的元素依次填入到temp中
        // 以下两个while只有一个会执行
        while(p1 <= mid) {
            temp[i++] = arr[p1++];
        }
        while(p2 <= R) {
            temp[i++] = arr[p2++];
        }
        // 把最终的排序的结果复制给原数组
        for(i = 0; i < temp.length; i++) {
            arr[L + i] = temp[i];
        }
    }
}/*public static String LinearSearch(Contact searchElement){
        boolean searchSuccess=false;
        String result="";
        for (int i=0;i<players.length;i++){
            if (players[i].compareTo(searchElement)==0){
                result="Found!At locate ["+i+"]";
                searchSuccess=true;
                break;
            }
        }
        if (searchSuccess==false){
            result="Not Found!";
        }
        return result;
    }

    public static String BinarySearch(Contact searchElement){
        //排序
        for (int i=0;i<players.length-1;i++){
            for (int j=i+1;j<players.length;j++){
                if (players[i].compareTo(players[j])>0){
                    Contact temp=players[i];
                    players[i]=players[j];
                    players[j]=temp;
                }
            }
        }

        //查找
        int high,low,middle;
        low=0;
        high=players.length-1;
        middle=(low+high)/2;
        boolean searchSuccess=false;
        String result="";

        while (searchSuccess==false){
            if (low>high){
                searchSuccess=false;
                result="Not Found!";
                break;
            }
            else {
                if (players[middle].compareTo(searchElement)==0){
                    searchSuccess=true;
                    result="Found!At locate ["+middle+"]";
                    break;
                }
                else if (searchElement.compareTo(players[middle])<0){
                    high=middle-1;
                }
                else if (searchElement.compareTo(players[middle])>0){
                    low=middle+1;
                }
                middle=(low+high)/2;
            }
        }*/