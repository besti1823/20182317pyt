package week8;

public class RunSearch {
    private static Contact[] players=new Contact[7];
    public static void main(String[] args) {

        players[0] = new Contact ("Rodger", "Federer", "610-555-7384");
        players[1] = new Contact ("Andy", "Roddick", "215-555-3827");
        players[2] = new Contact ("Maria", "Sharapova", "733-555-2969");
        players[3] = new Contact ("Venus", "Williams", "663-555-3984");
        players[4] = new Contact ("Lleyton", "Hewitt", "464-555-3489");
        players[5] = new Contact ("Eleni", "Daniilidou", "322-555-2284");
        players[6] = new Contact ("Serena", "Williams", "243-555-2837");

        Contact searchElement1=new Contact("Lleyton","","464-555-3489");
        Contact searchElement2=new Contact("","","190");

        //线性查找
        System.out.println("LinearSearch1:"+"\n"+LinearSearch(searchElement1));
        System.out.println("LinearSearch2:"+"\n"+LinearSearch(searchElement2));

        //二分法查找
        System.out.println("BinarySearch1:"+"\n"+BinarySearch(searchElement1));
        System.out.println("BinarySearch2:"+"\n"+BinarySearch(searchElement2));
    }

    public static String LinearSearch(Contact searchElement){
        boolean searchSuccess=false;
        String result="";
        for (int i=0;i<players.length;i++){
            if (players[i].compareTo(searchElement)==0){
                result="Found!At locate ["+i+"]";
                searchSuccess=true;
                break;
            }
        }
        if (searchSuccess==false){
            result="Not Found!";
        }
        return result;
    }

    public static String BinarySearch(Contact searchElement){
        //排序
        for (int i=0;i<players.length-1;i++){
            for (int j=i+1;j<players.length;j++){
                if (players[i].compareTo(players[j])>0){
                    Contact temp=players[i];
                    players[i]=players[j];
                    players[j]=temp;
                }
            }
        }

        //查找
        int high,low,middle;
        low=0;
        high=players.length-1;
        middle=(low+high)/2;
        boolean searchSuccess=false;
        String result="";

        while (searchSuccess==false){
            if (low>high){
                searchSuccess=false;
                result="Not Found!";
                break;
            }
            else {
                if (players[middle].compareTo(searchElement)==0){
                    searchSuccess=true;
                    result="Found!At locate ["+middle+"]";
                    break;
                }
                else if (searchElement.compareTo(players[middle])<0){
                    high=middle-1;
                }
                else if (searchElement.compareTo(players[middle])>0){
                    low=middle+1;
                }
                middle=(low+high)/2;
            }
        }
        return result;
    }
}
/*public static String LinearSearch(Contact searchElement){
        boolean searchSuccess=false;
        String result="";
        for (int i=0;i<players.length;i++){
            if (players[i].compareTo(searchElement)==0){
                result="Found!At locate ["+i+"]";
                searchSuccess=true;
                break;
            }
        }
        if (searchSuccess==false){
            result="Not Found!";
        }
        return result;
    }

    public static String BinarySearch(Contact searchElement){
        //排序
        for (int i=0;i<players.length-1;i++){
            for (int j=i+1;j<players.length;j++){
                if (players[i].compareTo(players[j])>0){
                    Contact temp=players[i];
                    players[i]=players[j];
                    players[j]=temp;
                }
            }
        }

        //查找
        int high,low,middle;
        low=0;
        high=players.length-1;
        middle=(low+high)/2;
        boolean searchSuccess=false;
        String result="";

        while (searchSuccess==false){
            if (low>high){
                searchSuccess=false;
                result="Not Found!";
                break;
            }
            else {
                if (players[middle].compareTo(searchElement)==0){
                    searchSuccess=true;
                    result="Found!At locate ["+middle+"]";
                    break;
                }
                else if (searchElement.compareTo(players[middle])<0){
                    high=middle-1;
                }
                else if (searchElement.compareTo(players[middle])>0){
                    low=middle+1;
                }
                middle=(low+high)/2;
            }
        }*/