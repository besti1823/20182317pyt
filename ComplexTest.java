import junit.framework.TestCase;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ComplexTest extends TestCase {
    Complex a = new Complex();
    Complex b = new Complex(2,-3);
    Complex c = new Complex(1.00,3.00);

    @Test
    public void testequals(){
        assertTrue(a.equals(new Complex(0,0)));
        assertFalse(b.equals(a));
    }

    @Test
    public void testtoString(){
        assertEquals("0.00",a.toString());
        assertEquals("2.00-3.00i",b.toString());
        assertEquals("1.00+3.00i",c.toString());
    }

    @Test
    public void testComplexAdd(){
        assertEquals("2.00-3.00i",a.ComplexAdd(b).toString());
        assertTrue(c.ComplexAdd(b).equals(b.ComplexAdd(c)));
    }

    @Test
    public void testComplexSub(){
        assertEquals("-1.00+6.00i",c.ComplexSub(b).toString());
        assertEquals("-2.00+3.00i",a.ComplexSub(b).toString());
    }

    @Test
    public void testComplexMul(){
        assertEquals("0.00",a.ComplexMulti(b).toString());
        assertEquals("11.00+3.00i",b.ComplexMulti(c).toString());
    }

    @Test
    public void testComplexDiv(){
        try {
            assertEquals("0.00", a.ComplexDiv(b).toString());
            assertEquals("-0.70-0.90i", b.ComplexDiv(c).toString());
        }catch (Complex.ComplexDivideZeroException e){
            System.exit(1);
        }
    }

    @Test
    public void testComplexDivException() {
        try{
            b.ComplexDiv(a);
            fail("Excepted an ComplexDivideZeroException to be thrown.");
        }catch (Complex.ComplexDivideZeroException e){
            assertThat(e.getMessage(),is("除数不能为0"));
        }

    }
}
